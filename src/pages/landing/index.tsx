import '../styles/landing/index.css'
import HomeImg from '../../assets/svg/Home'
// import AboutImg from '../../assets/svg/book-img.svg'
// import BookImg from '../../assets/landingimages/book-img.svg'
import Doc1 from '../../assets/landingimages/doc-1.jpg'
import Doc2 from '../../assets/landingimages/doc-2.jpg'
import Doc3 from '../../assets/landingimages/doc-3.jpg'
import Doc4 from '../../assets/landingimages/doc-4.jpg'
import Doc5 from '../../assets/landingimages/doc-5.jpg'
import Doc6 from '../../assets/landingimages/doc-6.jpg'
import Pic1 from '../../assets/landingimages/pic-1.png'
import Pic2 from '../../assets/landingimages/pic-2.png'
import Pic3 from '../../assets/landingimages/pic-3.png'
import Blog1 from '../../assets/landingimages/blog-1.jpg'
import Blog2 from '../../assets/landingimages/blog-2.jpg'
import Blog3 from '../../assets/landingimages/blog-3.jpg'
import {
  FaHeartbeat,
  FaChevronRight,
  FaBed,
  FaUserMd,
  FaUsers,
  FaHospital,
  FaNotesMedical,
  FaAmbulance,
  FaPills,
  FaFacebookF,
  FaInstagram,
  FaLinkedin,
  FaTwitter,
  FaPhone,
  FaEnvelope,
  FaMapMarker,
  FaPinterestP,
  FaHamburger,
} from 'react-icons/fa'
import { Link } from 'react-router-dom'
const Landing = () => {
  return (
    <>
      <header className='header'>
        <Link to='/' className='logo'>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              gap: '0.5vw',
              fontWeight: 'bold',
            }}
          >
            <i>
              <FaHeartbeat />
            </i>
            medcare
          </div>
        </Link>

        <nav className='navbar'>
          <a href='#home'>home</a>
          <a href='#services'>services</a>
          <a href='#about'>about</a>
          <a href='#doctors'>doctors</a>
          <a href='#book'>book</a>
          <a href='#review'>review</a>
          <a href='#blogs'>blogs</a>
          <Link to='/login'>Login</Link>
        </nav>

        <div id='menu-btn' className='fas fa-bars'>
          <i>
            <FaHamburger />
          </i>
        </div>
      </header>

      <section className='home' id='home'>
        <div className='image'>
          {/* <HomeImg /> */}
          {/* <img src={HomeImg} alt='' /> */}
          {/* <img src={HomeImg} alt='' /> */}
          <img
            src='https://images.unsplash.com/photo-1576091160550-2173dba999ef?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MjR8fGhvc3BpdGFsfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60'
            alt=''
          />
        </div>

        <div className='content'>
          <h3>stay safe, stay healthy</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem sed autem vero? Magnam, est
            laboriosam!
          </p>
          <Link to='#' className='btn'>
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                gap: '0.5vw',
                fontWeight: 'bold',
              }}
            >
              contact us
              <i>
                <FaChevronRight />
              </i>
            </div>
          </Link>
        </div>
      </section>

      {/* <!-- home section ends --> */}

      {/* <!-- icons section starts  --> */}

      <section className='icons-container'>
        <div className='icons'>
          <i>
            <FaUserMd />
          </i>
          <h3>140+</h3>
          <p>doctors at work</p>
        </div>

        <div className='icons'>
          <i>
            <FaUsers />
          </i>

          <h3>1040+</h3>
          <p>satisfied patients</p>
        </div>

        <div className='icons'>
          <i>
            <FaBed />
          </i>
          <h3>500+</h3>
          <p>bed facility</p>
        </div>

        <div className='icons'>
          <i>
            <FaHospital />
          </i>
          <h3>80+</h3>
          <p>available hospitals</p>
        </div>
      </section>

      {/* <!-- icons section ends --> */}

      {/* <!-- services section starts  --> */}

      <section className='services' id='services'>
        <h1 className='heading'>
          our <span>services</span>
        </h1>

        <div className='box-container'>
          <div className='box'>
            <i>
              <FaNotesMedical />
            </i>
            <h3>free checkups</h3>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ad, omnis.</p>
            <a href='#' className='btn'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '0.5vw',
                  fontWeight: 'bold',
                }}
              >
                learn more
                <FaChevronRight />
              </div>
            </a>
          </div>

          <div className='box'>
            <i>
              <FaAmbulance />
            </i>
            <h3>24/7 ambulance</h3>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ad, omnis.</p>
            <a href='#' className='btn'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '0.5vw',
                  fontWeight: 'bold',
                }}
              >
                learn more
                <FaChevronRight />
              </div>
            </a>
          </div>

          <div className='box'>
            <i>
              <FaUserMd />
            </i>
            <h3>expert doctors</h3>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ad, omnis.</p>
            <a href='#' className='btn'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '0.5vw',
                  fontWeight: 'bold',
                }}
              >
                learn more
                <FaChevronRight />
              </div>
            </a>
          </div>

          <div className='box'>
            <i>
              <FaPills />
            </i>
            <h3>medicines</h3>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ad, omnis.</p>
            <a href='#' className='btn'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '0.5vw',
                  fontWeight: 'bold',
                }}
              >
                learn more
                <FaChevronRight />
              </div>
            </a>
          </div>

          <div className='box'>
            <i>
              <FaBed />
            </i>
            <h3>bed facility</h3>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ad, omnis.</p>
            <a href='#' className='btn'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '0.5vw',
                  fontWeight: 'bold',
                }}
              >
                learn more
                <FaChevronRight />
              </div>
            </a>
          </div>

          <div className='box'>
            <i>
              <FaHeartbeat />
            </i>
            <h3>total care</h3>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ad, omnis.</p>
            <a href='#' className='btn'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '0.5vw',
                  fontWeight: 'bold',
                }}
              >
                learn more
                <FaChevronRight />
              </div>
            </a>
          </div>
        </div>
      </section>

      {/* <!-- services section ends --> */}

      {/* <!-- about section starts  --> */}

      <section className='about' id='about'>
        <h1 className='heading'>
          <span>about</span> us
        </h1>

        <div className='row'>
          <div className='image' style={{ border: '2px solid red' }}>
            <img
              src='https://images.unsplash.com/photo-1666214280465-a40313304801?ixlib=rb-4.0.3&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80'
              alt='sdsd'
            />
          </div>

          <div className='content'>
            <h3>we take care of your healthy life</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Iure ducimus, quod ex
              cupiditate ullam in assumenda maiores et culpa odit tempora ipsam qui, quisquam quis
              facere iste fuga, minus nesciunt.
            </p>
            <p>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Natus vero ipsam laborum
              porro voluptates voluptatibus a nihil temporibus deserunt vel?
            </p>
            <a href='#' className='btn'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '0.5vw',
                  fontWeight: 'bold',
                }}
              >
                learn more
                <FaChevronRight />
              </div>
            </a>
          </div>
        </div>
      </section>

      {/* about section ends */}

      {/* doctors section starts */}

      <section className='doctors' id='doctors'>
        <h1 className='heading'>
          our <span>doctors</span>
        </h1>

        <div className='box-container'>
          <div className='box'>
            <img src={Doc1} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>

          <div className='box'>
            <img src={Doc2} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>

          <div className='box'>
            <img src={Doc3} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>

          <div className='box'>
            <img src={Doc4} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>

          <div className='box'>
            <img src={Doc5} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>

          <div className='box'>
            <img src={Doc6} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>
          <div className='box'>
            <img src={Doc6} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>
          <div className='box'>
            <img src={Doc6} alt='' />
            <h3>john deo</h3>
            <span>expert doctor</span>
            <div className='share'>
              <FaFacebookF className='link' />
              <FaTwitter className='link' />
              <FaInstagram className='link' />
              <FaLinkedin className='link' />
            </div>
          </div>
        </div>
      </section>

      {/* <!-- doctors section ends -->

<!-- booking section starts   --> */}

      <section className='book' id='book'>
        <h1 className='heading'>
          <span>Get Started </span>With US
        </h1>

        <div className='row'>
          <div className='image'>
            <img
              src='https://images.unsplash.com/photo-1457433575995-8407028a9970?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8ODh8fGlzbyUyMGFwcHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60'
              alt='img'
            />
          </div>

          <form action='a'>
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                gap: '1vw',
              }}
            >
              <div>
                <Link to='#'>
                  <img
                    src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTADvR-88OM7uMP6_wEB9NDsCpyJf09HVc_iw&usqp=CAU'
                    alt='Download for anroid'
                  />
                </Link>
              </div>
              <div>
                <Link to='#'>
                  <img
                    src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZ0AAAB6CAMAAABTN34eAAAAflBMVEUAAAD///+mpqaoqKiOjo6IiIjOzs6SkpLU1NSqqqpJSUmenp77+/ttbW1SUlKioqLo6Ojd3d28vLyDg4PIyMivr6/x8fE2NjYYGBjt7e3h4eGYmJhAQEB5eXnCwsJXV1dmZmYmJiYfHx97e3sQEBAuLi5hYWFFRUUxMTE5OTlOxmQ3AAARK0lEQVR4nO1d62KqvBLlogiKIgh4q5TWatv3f8FDyOQeJGHr1v0d1p8WSEKSlcxMJkN0XII4zYvIG/FcRFl+oJS4Dvwts0Xief6IZ8NLkiSPRXbyhedH0WIyHfFcTJIi8pMk5NiJi8Qvju9fzojn47NOIi/JKDux70Xe/Nm1GkFRN9OnIOwUXjR7doVG8Lgkfjt7GnbyZCTn1bDz/CRA7JQL33t2ZUbI2BbeIm7Yybxi1Dmvh1OUVK4TL/zjs2syQoOomTxOmkTvz67ICA3OUZI6uReN65xXxDzycqfRPs+uxwgddoVXOJE/eXY9Rmjhe5Hj+dNnV2OEFl6z0hnZeVWM7LwyRnZeGSM7r4zHsDOPDu7oVv1zPIKd8xptTAxwDu0+Pu63LA5dN7HNs0Iu+7vgPiXdn535Gm+5Wq6hlnvYSF9n9V3q0bBjvcj+z7PjkWCSpV2+DQtDcct7uP1GdlSEtIt/7DI27MSLyfRY7AdMvI6a/HV26iy8U0kYd2YnZbFxljkbdtb4vysKFqr/uCpPYKdpw+E+JQHuy07GpFNmmZWx4zixPbkqnsDO6aXZWXK6w1Z18Oy8N/k3tNAizwuixM7TKd7FXU5xnXcT/M9kckaCJajOJB/Pzu8iDyrOxL/MMq7QtrwMJZjr+vTzWAXV8UIrOmle+F4Eudxpq2nhuvvNdPLrEHY2VZCtuCTnLAj8bV9XMNyVnZiRs+5PLYJnx2nMvgD/tyrBUMCkVK5btf80t74hW4qvSxCrZPRy7ORQqaN0vSb2+w7bmfFSw04hCYOmQlBAKZr/C9L2kwPsYDPUJwnm0D/mhv492Um4qWNpsUns+KizEdAsiqu2M9oxuIQH6H7b9xVYELEbB/Bq4JWxg7o+rFBXFfhG7q6rIueYRN1WHppXqewgMyfNUGBzSEsjAvwgpDzybW/YiUk09Ak/f3NRwC3Ka7xQvyc7HDlhf2oJAjsb0kuoQejvmmii5sau+RORvmlG8hf8dYMvPC3e2pSUnYbAGMklZOvX7Z1ry/SWyt8KuhSH9wk4QqINnXqoz9MPnLgWEy9FvYOkM5ove3yrxOm3OunZgTuyw69YPofkZuycoAUJmUMfLsyREA/FvRu0KX5If5Qke0x6kbBzabJe2zupNNqbYdwGiu2aFGeSR+o58t7W4CGl4IGypnORqzbPTjtFatIbGyIcI/PJc0d2AkaOtVyT2DlDV+yprghgPs7aLmlIubY9OiUJStKLOekEws6CFryEidfia3XNQYlNyRigo4KCKaIfQnLDTtTeiagQJdDabGSGNi34bW9cQR4Y4I7sMHLO/YkVCOwcYXwSddOy0t65tMmaq2aS5C0X2AQqyZAoiN1A2MmZQqey6KMAYyMXsqh2sM+EdAxDPiXj4AgGCYOWnRhq1vwNW6RKvk7cj50tJacekl1gJ8cNQPIMLFk6qvfob9iM2hzRFXNCvYOdlBlJZOCgqdko7RLYyaldpbCTsXF+ADk4lB1O7kvmRDfux84V3ow05gAI7Li4RxE7W/oYtzZC7COF29yZb4mUucFOyGxakLnI4IuaWk4YO6BAbrGzZ2wPZacg+Pt6By9F1wNUTgueHY/MGTYRI/K46drk2ra8UTmIIfy8m52KdvAv2HMhcDpjkg0UyFVmhyktVJfWNB7KTlND63F7P3be3TL0h4djc+wgMweripTqjJKO76YrvbZbUjcviDq/wc6U9lPCtFmrpxfAzpmm8GV20DTDfUpF6212SBtUdoIBnqUH7Vz/Xq9zO6uasYNmTklvYkNnxmRc3oyCVj8f0T/cCr6DHURGO1WQ4ezBjRPkyUmKdvL8quudPZlXNPENdmqWXWUHyZZvqy75A3bevRDZPWVwlP1G12JNlF9xNS6vIaKcTCeL1i7fE8MXrd9Pzm7hUgWDVToaz21fkhfcYGeCJP6HU5eE9LB1DK0OhJTWB5Oe60XrLJAagwy7LU6Mh9sNdhD/6eaIlscqO+1iqqjfVqfC2JUzkJ2Ec6m5B243ZlvwT9CejWGJ/Fo2YrdLco8tEVxuBepy6brYoZ4y7DKAdXyDivf6tNislXX8hNYK/Lo32IEl36ejZceh37nvDftkGDvUo0RR4TG8OShPOG/zTRB2ykDcecNdu+eWUCnpdrZO6WCHjFHYFacr+7odQVOHqYkEanpQvSzv2LlaEUF9IEv9hWbd0tKMhImwGiWW0gzXYx/J2bowgJ0fIrjESRJkqe6+a+50+9hudbrqu74OM9L5oq+1IPPnS9l+WSl3KL7el6uuZ+qLVrcCV77mqzfTopwh7NQdHNxA+ce9+38Ka3ZO9uQ0E2vXX/AIFbbsXPup0MFYD47gYcnOZRg5pqbBCBGW7GgNAgOM36UOgh07s2HclPabcSMQ7NgZSM5DW/BfhhU7ST8TOoz29FBYsRP3M6FB/cj6/7dhw86ynwkNqv6CR3TAhp1qEDvjQRXDYcPOIMFW9Jc7ogsW7PwOmjq/j27BfxkW7Gz6qVBhHU/tXPykhd+f9A74OQZ4Cyk+VNOXW5ZZsBMNYcdesNFgcfN91cF4lzY90vrx77SBBTu53PMmsPev0Q082y+A7JEp1X38O61gwU7X5tpNWE+AT5r1zz+w6oHGafhi/kALdgZ5QG12AltwrrwHd5WuPVKSz/qxVeiDBTt7TWt6YW2ycTP0scY4jQXJpst6uYlQSICwcv5ODgO+dLkrHs2Odfhhmws79B7qPSU7VdwQ2JQndnFsV3eBmvFv4tGSzTZwd9rm+tkPo9YCMHU6TyTBQVT/Dju6aKhe2J7HgjsFrCnjyKIBwKuc7uVY8I+xE+j7/zZMvyMiaDOFOODzoaKtb/D8a+yoqwMDWJrFG+izHc5tbfEZA7426v4O7F9jZ6Hr/V7UVtXBffJNlJx0/v/HGwJ2em9nWXhIq9lFSPHTpsD/rxZ5eggz/Xr4DddO3/CmiAsWseHljSsRcJkWQVNydZRiEL/apHivcVqlB+HNyyI4rA9BZLUAtGBnUCSbpU3aZkHzDSttSS0c6XjnXDAhb7SzQOYNjcDWOgA+8CO94FVbwT3ccNZRLMSrYzMQGYHYtuHUZsHlsTi47uE+aivD60S77B1nFr9vwHJvKjTWFZQHlr5vsmNDM2DjG7VT2sAU4FxeVyzlfBnVAdSPK21bro23vGz2d2w44epiWhWHuPJaZYB7T/yCAZOXKPYJW7MUQEYppTg5MojXUKfalDZQdjR++kjKl1EVQNhRVIJxaKwNO6H8FjNYnDCIM7T/4vEnUovHoOcrr6AkYnZq1SWozJ538kRjtim5iW2j3UQpxHzVB3kA7BDf1DoIiFA0HbE27AwMyTFfkeLex5oKQoKFeB4cYI+bGB6nM7aVTvRzwqWoZtMFm2XKy+ijUml90ADPvhL9G4Sgngij+00z+D9mZK+Yan/8WlqriM8UtkL6SxZ6t2HDDh1uj6IHt2vKNVUc2SyIm3zYTRQQidNmQgRMgc9QvObANuJjX91301nUsVQUmaKf/HOsmLLJebLkMtEJBt8+makeq4ipoeyYnjwiVByPMuHLfjo8WFeTT9NAsdDvvpg1SxSMElV34eMkAtk80LAD24+cnQeSijhPaYHcUMCzmbNc8XAxmzxW7AwLylFa2QUsuFLhSjhxh3xSyFMGdYJcRMbz7m2wstTfTtsJnsNAnD8adnA6fn1NzFjIStiJlEzcVPlub5i5QazYGRbQhmHgcMOzhc4zV7xsMIeyhM/Y4B6+mAlXGCAPdR+piJpU6AWVnYlaITI2wCoBdnidj11SgljFo8Voa+VvxFFj9Ls0ceOoBMIt5z/NnKutp4sLLNqAHXFnCCaPzoz9EMQBv72jskN+PJcHzOaUb4CgZrFcFfYR8SuNtvTt2BnkagP0DpZ3qethpnJCYa7jueZvztT+wWdEKDcJPiNO/XBkqOzgJJLvI+YpEy74W0KWRNOIDtix8zacnP6De7D9xQkOnJGrG7AjuS7xTayrgR3RAgCaO02TJVsesbWZws6bnIJLBkMvVviDKImMB96JMQpgtvy6alDkR4v+mYzbtl9TKI0Fdpa6fFi4ADtiAvBH3xit79Q+oLwq7ADH0tAAO+7K1YRXMt2LEKOtFUt2BtsF/TsJq66sLImeHaxWsETUsgPbETfDFEiwHk2ksAN+AsnpACY81nqYHX4UnDs7xMiKtf2qd1BwgWtygE9nMCObdQPZcaSO1wLMNzqKFHagaClQCO52sdMdP/uIuTN48vSXLDsuKVgP3WIH6zUtOyD8e/QwjDuyX6SwAwa1xM6Cr5INO0YBR9bnFQyKLjBwFnx3Z6Zp9Oy4XD9q2YGie1Zc0Ps1XCrswPaW5O0GVxL2NKjs4EyD4yat2SErQisYrIzht0cuW4YPaDpVxHNdB8HMwGNRy46+X2WsROoVduC5JKKFRZDKzkpXH3PYn8QyxJ1jENUJnmfhHrCRS9fiMUcn/iawI56DBiT3LLikiWm43sE3YfSp7MBz42N2JAw4xcj+IyuD2PGtruOJLiKX0IHiUgG8nHgXbaYrRiqlA0uxIwOFi7WmmKVQIw070mH7lhjAjvVhLCYeP9CukiMZRj0Zz0Sq8kk+hXcAO8LSFyZXn5GUiWUHSkFg1QnqKxQqqGGnMBoZXRhyPpu4q98Pk3mNjQ15zxAWc2SyEHZ4ewcW69AlxAvKGw4wdWqxaHlHByYvdetJZDk0UIS/BQOVaH0NO29iCywx6PQ8u5hdE48StFzZ5BY9V9QiYQqebOiAi5N+wcDCRYA+2UVdHQRl+ANvoourhXTtUI3L6SLIRLpP116YXcMi9gexw76xMYDRydjQyUqEDIxgmArMXkzE57RH2PclQOAH8T3JPlD06xEJmdUXIg6YECYLO27ig8+BToQdzEo643XskC2gNTcYtpHhARvDzgW1UD2x0R5tKvUNATigwazgrPk4m2xYXAHNyH39s4+mmwUNVJG1DvHnrsMgCNlCmBse5NYhD9ZgZRC/TDlp1qzfNPjkR8wiyQpapX0xXS7PkyiIe617goGntpp/4WsUbPslkMADuMAXwI7qTaJybNaRQtl6U482ReBdnHxYFrEEtN4m5nnD17Ik1227GAq6oSceT+X3lWGWValibZvFGkrrdA453wPATi07fdhLgB05MFLdF9U62wXpx5dB9aGGHs4tim8oetZTMxn+EMLg86gFh1u6IfLrzeMJKi83y6DQbTtigDmMZxVZMH4J9Kw5IU58BaLnXmNMT9VVWyiZcVxkMhvqsp8x5TN1sONcVR+iWb8MP8udxiK5hfjTokv6wDTOENSt1qnOt4Yt55lRL0ajUU/OJ/tCPNUHlp+F4Mk4U89ZX1HblBe5C47XsNbUVSe1zsJcDU1/TvVPTtr/jsJDENXqg69NVFWRoeZD6T93CPpn7SM8Qnlny6kIQvUzAM7P9jnNgjTIJjd+lXY19bIqzzNv03EE/ntS5VW2qKVss6bkMI9OssHTVnXXYQbVxyLLsuh4svjs5UG/g/AY6H3UPPT7O/8sRnZeGSM7r4yRnVfGyM4rY2TnlTGy88oY2XlljOy8MkZ2Xhn/FDsQf3TDRXT877ET+aZeuRF/F74XOYVn/TOlI/4GdoVXOLkXjQeuvyLmkZc7aRK92GmlI1psouTgxAvf9pC7EX8DkbdwHTfzikce8jhiGE5RUjXslAtf/Zp/xJPx21hrccOOmyejbHs1fHp+ErqIHbfwopGel8JPQw6KhEPsxFGzLB36lcmI+2MZ+UkbeIQDfYrEjxbX8RePXwGXpRd5CY4hJccxLDw/KpLZZMRzMfOKZuIkJAITYuDKbJF4nj/i2fCSJMlJTCM7myhO8yLyRjwXUZZzH7b/D0+Q6d6tJW1EAAAAAElFTkSuQmCC'
                    alt='Download for iso'
                  />
                </Link>
              </div>
            </div>
          </form>
        </div>
      </section>

      {/* <!-- booking section ends -->

<!-- review section starts  --> */}

      <section className='review' id='review'>
        <h1 className='heading'>
          {' '}
          client<span>review</span>{' '}
        </h1>

        <div className='box-container'>
          <div className='box'>
            <img src={Pic1} alt='' />
            <h3>john deo</h3>
            <div className='stars'>
              <i>
                <FaFacebookF />
              </i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star-half-alt'></i>
            </div>
            <p className='text'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam sapiente nihil
              aperiam? Repellat sequi nisi aliquid perspiciatis libero nobis rem numquam nesciunt
              alias sapiente minus voluptatem, reiciendis consequuntur optio dolorem!
            </p>
          </div>

          <div className='box'>
            <img src={Pic2} alt='' />
            <h3>john deo</h3>
            <div className='stars'>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star-half-alt'></i>
            </div>
            <p className='text'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam sapiente nihil
              aperiam? Repellat sequi nisi aliquid perspiciatis libero nobis rem numquam nesciunt
              alias sapiente minus voluptatem, reiciendis consequuntur optio dolorem!
            </p>
          </div>

          <div className='box'>
            <img src={Pic3} alt='' />
            <h3>john deo</h3>
            <div className='stars'>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star'></i>
              <i className='fas fa-star-half-alt'></i>
            </div>
            <p className='text'>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam sapiente nihil
              aperiam? Repellat sequi nisi aliquid perspiciatis libero nobis rem numquam nesciunt
              alias sapiente minus voluptatem, reiciendis consequuntur optio dolorem!
            </p>
          </div>
        </div>
      </section>

      {/* <!-- review section ends -->

<!-- blogs section starts  --> */}

      <section className='blogs' id='blogs'>
        <h1 className='heading'>
          our <span>blogs</span>
        </h1>

        <div className='box-container'>
          <div className='box'>
            <div className='image'>
              <img src={Blog1} alt='' />
            </div>
            <div className='content'>
              <div className='icon'>
                <a href='#'>
                  <i className='fas fa-calendar'></i> 1st may, 2021{' '}
                </a>
                <a href='#'>
                  <i className='fas fa-user'></i> by admin
                </a>
              </div>
              <h3>blog title goes here</h3>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Provident, eius.</p>
              <a href='#' className='btn'>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '0.5vw',
                    fontWeight: 'bold',
                  }}
                >
                  learn more
                  <FaChevronRight />
                </div>
              </a>
            </div>
          </div>

          <div className='box'>
            <div className='image'>
              <img src={Blog2} alt='' />
            </div>
            <div className='content'>
              <div className='icon'>
                <a href='#'>
                  <i className='fas fa-calendar'></i> 1st may, 2021{' '}
                </a>
                <a href='#'>
                  <i className='fas fa-user'></i> by admin{' '}
                </a>
              </div>
              <h3>blog title goes here</h3>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Provident, eius.</p>
              <a href='#' className='btn'>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '0.5vw',
                    fontWeight: 'bold',
                  }}
                >
                  learn more
                  <FaChevronRight />
                </div>
              </a>
            </div>
          </div>

          <div className='box'>
            <div className='image'>
              <img src={Blog3} alt='' />
            </div>
            <div className='content'>
              <div className='icon'>
                <a href='#'>
                  <i className='fas fa-calendar'></i> 1st may, 2021{' '}
                </a>
                <a href='#'>
                  <i className='fas fa-user'></i> by admin{' '}
                </a>
              </div>
              <h3>blog title goes here</h3>
              <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Provident, eius.</p>
              <a href='#' className='btn'>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '0.5vw',
                    fontWeight: 'bold',
                  }}
                >
                  learn more
                  <FaChevronRight />
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>

      {/* <!-- blogs section ends -->

<!-- footer section starts  --> */}

      <section className='footer'>
        <div className='box-container'>
          <div className='box'>
            <h3>quick links</h3>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaChevronRight />
                </i>
                home
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaChevronRight />
                </i>
                services
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaChevronRight />
                </i>
                about
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaChevronRight />
                </i>{' '}
                dactors
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaChevronRight />
                </i>{' '}
                book
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaChevronRight />
                </i>
                review
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaChevronRight />
                </i>
                blogs
              </div>
            </a>
          </div>

          {/* <div className='box'>
            <h3>our services</h3>
            <a href='#'>
              {' '}
              <i className='fas fa-chevron-right'></i> dental care{' '}
            </a>
            <a href='#'>
              {' '}
              <i className='fas fa-chevron-right'></i> message therapy{' '}
            </a>
            <a href='#'>
              {' '}
              <i className='fas fa-chevron-right'></i> cardioloty{' '}
            </a>
            <a href='#'>
              {' '}
              <i className='fas fa-chevron-right'></i> diagnosis{' '}
            </a>
            <a href='#'>
              {' '}
              <i className='fas fa-chevron-right'></i> ambulance service{' '}
            </a>
          </div> */}

          <div className='box'>
            <h3>contact info</h3>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaPhone />
                </i>
                +123-456-7890
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaPhone />
                </i>
                +123-456-7890
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaEnvelope />
                </i>
                shaikhanas@gmail.com
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaEnvelope />
                </i>
                shaikhanas@gmail.com
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaMapMarker />
                </i>
                mumbai, india - 400104
              </div>
            </a>
          </div>

          <div className='box'>
            <h3>follow us</h3>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaFacebookF />
                </i>
                facebook
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaTwitter />
                </i>
                twitter
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaInstagram />
                </i>
                instagram
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaLinkedin />
                </i>
                linkedIn
              </div>
            </a>
            <a href='#'>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  gap: '0.1vw',
                  fontSize: '1.5rem',
                  fontWeight: 'bold',
                }}
              >
                <i>
                  <FaPinterestP />
                </i>
                pintest
              </div>
            </a>
          </div>
        </div>
      </section>
    </>
  )
}

export default Landing
