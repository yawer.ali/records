// import bgImage from '../assets/bg.png'

export default function Auth({ children }: any) {
  return (
    <>
      <main>
        <div className='relative w-full h-full py-40 min-h-screen'>
          <div
            className='absolute top-0 w-full h-full bg-blueGray-800 bg-no-repeat bg-full'
            style={{
              // backgroundImage: `url(${bgImage})`,
              backgroundColor: '#F4F5FA',
            }}
          ></div>
          {children}
        </div>
      </main>
    </>
  )
}
