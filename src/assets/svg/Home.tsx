import * as React from 'react'
import { SVGProps } from 'react'

const SvgComponent = (props: SVGProps<SVGSVGElement>) => (
  <svg
    width='20vw'
    height='20vw'
    viewBox='0 0 18 18'
    fill='none'
    xmlns='http://www.w3.org/2000/svg'
    {...props}
  >
    <path d='M1 8v8a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V8' stroke='#000' strokeLinecap='round' />
    <path
      d='m14.608 2.996-1.532-1.173A4 4 0 0 0 10.645 1h-3.29a4 4 0 0 0-2.431.823L3.392 2.996A1 1 0 0 0 3 3.79V5a10 10 0 0 0 3.456 7.562l1.235 1.07a2 2 0 0 0 2.618 0l1.235-1.07A10 10 0 0 0 15 5.001V3.79a1 1 0 0 0-.392-.794Z'
      stroke='#000'
    />
    <circle cx={9} cy={7} r={4} stroke='#000' />
    <path d='m7 7 1.5 1.5L11 6' stroke='#000' strokeLinecap='round' strokeLinejoin='round' />
    <path d='M3 14h2M13 14h2' stroke='#000' strokeLinecap='round' />
  </svg>
)

export default SvgComponent
