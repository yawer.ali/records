import { Link } from 'react-router-dom'

export default function Sidebar() {
  return (
    <>
      <nav className='md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-nowrap md:overflow-hidden shadow-xl bg-white flex flex-wrap items-center justify-between relative md:w-64 z-10 py-4 px-6'>
        <div className='md:flex-col md:items-stretch md:min-h-full md:flex-nowrap px-0 flex flex-wrap items-center justify-between w-full mx-auto'>
          <Link to='/dashboard'>
            <div className='md:block text-left md:pb-2 text-blueGray-600 mr-0 inline-block whitespace-nowrap text-sm uppercase font-bold p-4 px-0'>
              MedCare
            </div>
          </Link>
          <div
            className={
              'md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded '
            }
          >
            {/* Navigation */}
            <ul className='md:flex-col md:min-w-full flex flex-col list-none md:mb-4'>
              {/* <li className='items-center'>
                <Link to='/dashboard'>Dashboard</Link>
              </li> */}

              <li>
                <div className='flex items-center p-2 text-base rounded-lg  bg-gray-100 dark:hover:bg-gray-500'>
                  <svg
                    aria-hidden='true'
                    className='w-6 h-6 text-gray-500 transition duration-75 dark:text-gray-400 group-hover:text-gray-900 dark:group-hover:text-white'
                    fill='currentColor'
                    viewBox='0 0 20 20'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path d='M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z'></path>
                    <path d='M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z'></path>
                  </svg>
                  <Link to='/dashboard' className='ml-3'>
                    Dashboard
                  </Link>
                </div>
              </li>

              <li className='items-center'>
                <Link to='/appointment'>Appointments</Link>
              </li>
            </ul>

            {/* Heading */}
            <h6 className='md:min-w-full text-blueGray-500 text-xs uppercase font-bold block pt-1 pb-4 no-underline'>
              Account
            </h6>
            {/* Navigation */}
            <ul className='md:flex-col md:min-w-full flex flex-col list-none md:mb-4'>
              <li className='items-center'>
                <Link to='/profile'>Profile Page</Link>
              </li>
              <li className='items-center'>
                <Link to='/settings'>Settings</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  )
}
